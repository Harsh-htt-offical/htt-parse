const Papa = require('papaparse');
const yup = require('yup');

module.exports = {
  parseCSV: (csvString, config, userSchema) => {
    
    const { data, errors } = Papa.parse(csvString, config);

    if (errors.length > 0) {
      return { valid: false, errors };
    }

    if (userSchema && config.header === true) {
        const schema = yup.object(userSchema);
        const validData = [];
        const invalidData = [];
  
        data.forEach((rowData, rowIndex) => {
          try {
            schema.validateSync(rowData);
            validData.push(rowData);
          } catch (validationError) {
            invalidData.push({ rowIndex, rowData, errors: validationError.errors });
          }
        });
  
        return { valid: invalidData.length === 0, validData, invalidData };
    }

    return { valid: true, validData: data, invalidData:[] };
  },
};


