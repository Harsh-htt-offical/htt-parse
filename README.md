# CSV Parse Component

This is a simple package that provides a CSV parsing and validation utility. It uses the Papaparse library for parsing CSV data and supports optional validation using the Yup schema validation library.

## Installation

To install this package, use npm:

```bash
npm install csv-parse-component
```

## Usage

Here's how you can use the parseCSV function provided by this package:

```
const CsvParseComponent = require("csv-parse-htt");
const yup = require("yup");

const csvString = "Name,Age\nJohn,25\nJane,30\nAlice,28\nMartin,25\nRob,32";
const config = {
  header: true,
};

const userSchema = {
  Name: yup.string().required(),
  Age: yup.number().required().positive().integer(),
};

const { valid, validData, invalidData } = CsvParseComponent.parseCSV(
  csvString,
  config,
  userSchema
);
if (!valid) {

  console.log("Data is valid:", validData);

  console.error("Validation errors:", invalidData);

} else {

  console.log("All Data is valid:", validData);
}
```

## API

## `parseCSV(csvString, config, userSchema)`

Parses a CSV string and performs optional validation using a provided schema.

| Parameter    | Description |
| ------------ | ----------- |
| csvString    | The CSV string to parse. |
| config       | An object with parsing configuration options for Papaparse. For example, `{ header: true }` if your CSV data includes a header row. |
| userSchema   | An optional Yup schema for validation. |


Returns an object with the following properties:


| Property     | Description |
| ------------ | ----------- |
| valid        | A boolean indicating if the CSV data passed validation. |
| validData    | An array of valid data rows. |
| invalidData  | An array of objects containing information about invalid data rows, including the row index, row data, and validation errors. |

